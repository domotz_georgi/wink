'use strict';
var request = require('request'),
    express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    app = express();

// Constants //
var WINK_CLIENT_ID = "quirky_wink_android_app",
    WINK_CLIENT_SECRET = "e749124ad386a5a35c0ab554a4f2c045",
    TOKEN_AUTH_URL = "https://winkapi.quirky.com/oauth2/token",
    USER = "domenico@crapanzano.com",
    PASS = "D0m0tz123"

// Configure Express app
app.use(cookieParser('cookie_secret_shh')); // Change for production apps
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static(__dirname + '/app'));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));

app.get('/auth/wink', function(req, res){
    var postData = {
        "client_id": WINK_CLIENT_ID,
        "client_secret": WINK_CLIENT_SECRET,
        "username": USER,
        "password": PASS,
        "grant_type": "password"
    };
    var options = {
        uri: TOKEN_AUTH_URL,
        method: 'POST',
        body: postData,
        json: true
        };
    request(options, function (err, response) {
        //var json_response = JSON.parse(response.body);
        console.log(response.body.access_token);
        res.cookie('wink_token', response.body.access_token);
        res.redirect('/');
    });
});

app.post('/switch', function (req, res) {
    var token = req.cookies.wink_token,
        command = req.query.command,
        device_id = req.query.device_id,
        requestData = {
            "desired_state": {
                "powered": (command == 'on')
            }
        };
    console.log(device_id)
    var access_url = 'https://api.wink.com/binary_switches/' + device_id  + '/desired_state'

    var options = {
        url: access_url,
        method: 'PUT',
        headers:{
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify(requestData)
    };
    request(options, function (err, response) {
        //console.log('err ' + err);
        //console.log('res1 ' + JSON.stringify(response));
    });
});


module.exports = app;
