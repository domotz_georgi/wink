module.exports = function( grunt ) {
  'use strict';

  grunt.loadNpmTasks('grunt-wiredep');

  /**
    Node package info
  */
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    wiredep: {
      app: {
        src: 'index.html'
      }
    }
  });

  /**
    Start the web server on port 3000
  */
  grunt.registerTask('server', 'Start express server', function() {
    require('./server.js').listen(3000, function () {
      grunt.log.writeln('Web server running at http://localhost:3000.');
    }
    ).on('close', this.async());
  });

  /**
    Set the server task as our default.
  */
  grunt.registerTask('default', ['server']);
};
