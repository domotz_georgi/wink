
/* globals $, Firebase */
'use strict';

var winkToken  = $.cookie('wink_token'),
    url        = "https://api.wink.com/users/me/wink_devices";


if (winkToken) { // Simple check for token

} else {
  // No auth token, go get on
  window.location.replace('/auth/wink');
}



initiateListener();

function initiateListener(){
    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            'Authorization': 'Bearer ' + winkToken
        },
        complete: function(data) {
            var devices = JSON.parse(data.responseText).data;
            for (var item in devices){
                initTable(devices[item]);
            }
        }
    });
}


function subscribeToDevice(key, channel){

    var pubnub_event = PUBNUB.init({
        subscribe_key: key
    });

    pubnub_event.subscribe({
        channel: channel,
        message: function(event){
            updateView(JSON.parse(event));
        }
    });

}

function initTable(device){
    var device_name = device.name,
        device_capabilities = JSON.stringify(device.capabilities.fields, null, 4),
        device_readings = JSON.stringify(device.last_reading, null, 4),
        channel = device.subscription.pubnub.channel,
        key = device.subscription.pubnub.subscribe_key;

    subscribeToDevice(key, channel);
    checkCapabilities(device_name, device.capabilities.fields, device.binary_switch_id);

    var row = "" +
        "<tr>" +
        "<td>" + device_name         + "</td>" +
        "<td>" + device_capabilities + "</td>" +
        "<td>" + device_readings     + "</td>" +
        "</tr>";
    $('#eventsTable').append(row);
}

function updateRow(m){
    var device_name = m.name,
        device_capabilities = JSON.stringify(m.capabilities, null, 4),
        device_readings = JSON.stringify(m.last_reading, null, 4);

    return "" +
        "<tr>" +
        "<td>" + device_name         + "</td>" +
        "<td>" + device_capabilities + "</td>" +
        "<td>" + device_readings     + "</td>" +
        "</tr>";
}
function updateView(m){
    var row = updateRow(m),
        device_name = m.name,
        old_row = $('#eventsTable tr > td:contains('+ device_name +')');
    if (old_row.length){
        old_row.parent().replaceWith(row);
    } else {
        $('#eventsTable').append(row);
    }
}


function checkCapabilities(dev_name, dev_cap, dev_id){
    var mutability;
    for (var item in dev_cap){
        mutability = dev_cap[item].mutability;
        if (mutability == "read-write"){
            drawButton(dev_name, dev_id, 'on');
            drawButton(dev_name, dev_id, 'off');
        }
    }
}

function drawButton(name,device_id, command){
    var inputElement = document.createElement('input');
    inputElement.type = "button";
    inputElement.value = name + ' ' + command;
    inputElement.addEventListener('click', function(){
        updateSwitch(device_id, command);
    });
    document.body.appendChild(inputElement);
}


function updateSwitch(id, command){
    var URI = "http://localhost:3000/switch?command=" + command + "&device_id=" + id
    $.ajax({
        url: URI,
        type: 'POST',
    });
}